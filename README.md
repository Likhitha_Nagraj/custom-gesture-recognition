# **Custom Gesture Recognition for Custom Tasks**
- **Idea**:Current systems allow only the pre-defined gestures,our product will allow the user to define their own custom gestures and use them for a custom task.

- **Objective**:Users should be able to control and manipulate the dynamic digital content by simply making their own customized gestures.

- **Area of Application**:From Personal computers to Digital signages.

- Our End product will be **"First of its Kind"**  in the Market.

-  [**Brief idea of Final Product:User Experience**](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/10)

- **End Product-1 : Desktop Application**

## Maintainer and Contributors:
- **Maintainer**: **Atharva Kale @atharvakale31**


## How to Contribute:
- Anyone is welcomed to contribute
- Pick the domain you want to contribute in from the issues.
- Fork the project and start the work:-)
- Submit a Merge Request.
- Your code will get reviewed and then merged into project.        
- Feel free to add your own ideas in issues.

## Tasks:

![Watch the video](https://media.giphy.com/media/YRtvWvv1VeN41zNVye/giphy.gif)

*courtesy:roadtovr.com*
- [1A:Define required features of Data(Pre defined gestures)](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/1) Easy
- [1B:create labeling documentation for data](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/2)
- [1C:Validation of data](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/3)
- [2A:Hand Gesture Estimation API.(for Custom Gesture)](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/4)
- [3A: Model Training](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/5)
- [3B:Testing and evaluation of Model.](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/6)
- [4A: Software Development and Model deployment](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/7)
- [4B:Deployment of Software on PC.](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/8)
- [5A:Final completion of the Project](https://gitlab.com/iotiotdotin/project-internship-ai/products/custom-gesture-recognition/-/issues/9)

## Perks:
1. Get a Project Intership Certificate for even a single contribution that gets accepted into Main Project.
2. Contributors with major contribution in a specific Domain will be assigned as Domain Lead for that particular domain.
3. Open Source Project.



